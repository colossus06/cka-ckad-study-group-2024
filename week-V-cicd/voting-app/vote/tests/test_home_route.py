from app import app


def test_voting_app():
    response = app.test_client().get('/')
    print(response.status_code)
    print(response.data)
    assert response.status_code == 200

def test_homepage_content():
    response = app.test_client().get('/')
    assert b'CKA' in response.data
    assert b'CKAD' in response.data

    # Add more assertions based on your template content