const request = require('supertest');
const assert = require('assert');
const app = require('./server'); // Import the Express app


describe('GET /', () => {
  it('should include the text "CKA"', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .end((err, res) => {
        assert.ok(res.text.includes('CKA'));
        done(err);
      });
  });
});

describe('GET /', () => {
  it('should include the text "CKA"', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .end((err, res) => {
        assert.ok(res.text.includes('CKA'));
        done(err);
      });
  });
});

describe('GET /', () => {
  it('shouldnt include the text "CKS"', (done) => {
    request(app)
      .get('/')
      .expect(200)
      .end((err, res) => {
        assert.ok(!res.text.includes('CKS') && !res.text.includes('KCNA'));
        done(err);
      });
  });
});